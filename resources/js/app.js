import helperMixin from "./mixins/helperMixin";

require('./bootstrap');

import axios from 'axios';
import Vue from 'vue';
import App from './App.vue';
import VueAuth from '@websanova/vue-auth';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import pagination from 'laravel-vue-pagination';

import auth from './auth';
import router from './router';

Vue.mixin(helperMixin);
window.Vue = Vue;

Vue.router = router;
Vue.use(VueRouter);

Vue.use(VueAxios, axios);
axios.defaults.baseURL = "/api/";
Vue.use(VueAuth, auth);

Vue.component('pagination', pagination);

App.router = Vue.router;
new Vue(App).$mount('#app');
