import VueRouter from 'vue-router';

import Register from './views/Register.vue';
import Login from './views/Login.vue';

import Books from './views/Books.vue';
import BooksCreate from './views/BooksCreate.vue';
import BooksEdit from './views/BooksEdit.vue';
import BooksView from './views/BooksView.vue';
import Creators from './views/Creators.vue';
import CreatorsCreate from './views/CreatorsCreate.vue';
import CreatorsEdit from './views/CreatorsEdit.vue';
import CreatorsView from './views/CreatorsView.vue';
import CreatorTypes from './views/CreatorTypes.vue';
import CreatorTypesCreate from './views/CreatorTypesCreate.vue';
import CreatorTypesEdit from './views/CreatorTypesEdit.vue';
import PageNotFound from './views/PageNotFound.vue';

const routes = [{
    path: '*',
    component: PageNotFound
}, {
    path: '/',
    name: 'home',
    redirect: {name: 'books'}
}, {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
        auth: false
    }
}, {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
        auth: false
    }
}, {
    path: '/books',
    name: 'books',
    component: Books,
    meta: {
        auth: true
    }
}, {
    path: '/books/create',
    name: 'book_create',
    component: BooksCreate,
    meta: {
        auth: true
    }
}, {
    path: '/books/:id',
    name: 'book_view',
    component: BooksView,
    meta: {
        auth: true
    }
}, {
    path: '/books/:id/edit',
    name: 'book_edit',
    component: BooksEdit,
    meta: {
        auth: true
    }
}, {
    path: '/creators',
    name: 'creators',
    component: Creators,
    meta: {
        auth: true
    }
}, {
    path: '/creators/create',
    name: 'creator_create',
    component: CreatorsCreate,
    meta: {
        auth: true
    }
}, {
    path: '/creators/:id',
    name: 'creator_view',
    component: CreatorsView,
    meta: {
        auth: true
    }
}, {
    path: '/creators/:id/edit',
    name: 'creator_edit',
    component: CreatorsEdit,
    meta: {
        auth: true
    }
}, {
    path: '/creator_types',
    name: 'creator_types',
    component: CreatorTypes,
    meta: {
        auth: true
    }
}, {
    path: '/creator_types/create',
    name: 'creator_type_create',
    component: CreatorTypesCreate,
    meta: {
        auth: true
    }
}, {
    path: '/creator_types/:id/edit',
    name: 'creator_type_edit',
    component: CreatorTypesEdit,
    meta: {
        auth: true
    }
},];

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});
export default router
