import LayoutMain from '../layouts/Main';
import LayoutAuth from '../layouts/Auth';

export default {
    created: function () {
        if (this.$auth.check()) {
            this.$emit('update:layout', LayoutMain);
        } else {
            this.$emit('update:layout', LayoutAuth);
        }
    },
}
