export default {
    methods: {
        formatErrors(errors) {
            return errors.join(', ');
        }
    }
}
