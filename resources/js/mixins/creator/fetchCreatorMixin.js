export default {
    mounted() {
        this.fetchCreator();
    },
    data: function () {
        return {
            creator: {}
        }
    },
    methods: {
        fetchCreator() {

            const url = '/creators/' + this.$route.params.id;

            axios.get(url).then(({data}) => {
                this.creator = data.data;
            })
        },
    },
}
