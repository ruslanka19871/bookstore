export default {
    mounted() {
        this.fetchCreatorTypes();
        this.fetchCreators();
    },
    data: function () {
        return {
            creator_types: {},
            creators: {},
        }
    },
    methods: {
        fetchCreatorTypes() {

            const url = '/creator_types/names';

            axios.get(url).then(({data}) => {
                this.creator_types = data.data;
            })

        },
        fetchCreators() {

            const url = '/creators/names';

            axios.get(url).then(({data}) => {
                this.creators = data.data;
            })

        },
    },
}
