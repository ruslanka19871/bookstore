export default {
    mounted() {
        this.fetchBook();
    },
    data: function () {
        return {
            book: {}
        }
    },
    methods: {
        fetchBook() {

            const url = '/books/' + this.$route.params.id;

            axios.get(url).then(({data}) => {
                this.book = data.data;
            })
        },
    },
}
