<!DOCTYPE html>
<html lang="en">

<head>
    <title>Книжный магазин</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="/css/material-kit.css?v=2.0.6" rel="stylesheet" />
</head>

<body class="sidebar-collapse">

<div id="app"></div>

<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>
