<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function(){

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::middleware('auth:api')->group(function () {

        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');

    });

});

Route::middleware('auth:api')->group(function () {
    Route::get('creators/names', 'CreatorController@getNames');
    Route::get('creator_types/names', 'CreatorTypeController@getNames');

    Route::post('books/{book}', 'BookController@update'); //php не умеет корректно получать multipart/form-data через put и patch

    Route::resource('books', 'BookController')->except(['create', 'edit']);
    Route::resource('creators', 'CreatorController')->except(['create', 'edit']);
    Route::resource('creator_types', 'CreatorTypeController')->except(['create', 'edit']);

});

Route::get('{path}', function() {
    return response()->json(['message' => 'Not Found'], 404);
})->where('path', '.*');

