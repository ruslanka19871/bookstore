<?php

use App\CreatorType;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Book::class, 50)->create()->each(function($book){
            $book->creators()->attach(factory(\App\Creator::class)->create(), ['creator_type_id' => CreatorType::AUTHOR_ID]);
        });
    }
}
