<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(2),
        'description' => $faker->text,
        'isbn' => $faker->isbn13,
        'year' => $faker->year,
        'cover' => $faker->imageUrl(),
    ];
});
