<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCreatorTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creator_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('sort');
            $table->timestamps();
        });

        $this->seed();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creator_types');
    }

    private function seed()
    {
        DB::table('creator_types')->insert([
            ['name' => 'Автор', 'sort' => 10],
            ['name' => 'Соавтор', 'sort' => 20],
            ['name' => 'Переводчик', 'sort' => 30],
        ]);
    }
}
