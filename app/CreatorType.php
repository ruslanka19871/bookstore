<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreatorType extends Model
{

    protected $fillable = ['name', 'sort'];

    const AUTHOR_ID = 1;

    //
}
