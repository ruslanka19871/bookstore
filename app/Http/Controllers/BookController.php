<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Resources\Book as BookResource;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(Book::with('authors:name')->paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fields = json_decode($request->get('fields'), 1);
        if ($request->file('cover')) {
            $fields['cover'] = $request->file('cover');
        }

        $validator = Validator::make($fields, [
            'title' => 'required|max:255',
            'isbn' => 'required|isbn',
            'year' => 'integer|bookyear',
            'cover' => 'image'
        ]);
        $validatedData = $validator->validate();

        $book = Book::create($validatedData);

        $creators = json_decode($request->get('creators'), 1);
        if ($request->get('creators')) {
            foreach ($creators as $bookCreator) {
                $book->creators()->attach($bookCreator['creator'], ['creator_type_id' => $bookCreator['creator_type']]);
            }
        }
        $book->save();

        return $this->successResponse($book->refresh());

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return $this->successResponse(new BookResource($book->load('creators')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $fields = json_decode($request->get('fields'), 1);
        unset($fields['cover']);

        if ($request->file('cover')) {
            $fields['cover'] = $request->file('cover');
        }

        $validator = Validator::make($fields, [
            'title' => 'max:255',
            'isbn' => 'isbn',
            'year' => 'integer|bookyear',
            'cover' => 'image'
        ]);
        $validatedData = $validator->validate();

        $book->update($validatedData);

        $creators = json_decode($request->get('creators'), 1);
        if ($request->get('creators')) {
            $book->creators()->detach();
            foreach ($creators as $bookCreator) {
                $book->creators()->attach($bookCreator['creator'], ['creator_type_id' => $bookCreator['creator_type']]);
            }
        }
        $book->save();

        return $this->successResponse($book->refresh());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return $this->successResponse($book);
    }
}
