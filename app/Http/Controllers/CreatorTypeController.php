<?php

namespace App\Http\Controllers;

use App\CreatorType;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CreatorTypeController extends Controller
{

    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(CreatorType::orderBy('sort', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'sort' => 'required|integer',
        ]);

        $creatorType = CreatorType::create($validatedData);
        return $this->successResponse($creatorType);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\CreatorType $creatorType
     * @return \Illuminate\Http\Response
     */
    public function show(CreatorType $creatorType)
    {
        return $this->successResponse($creatorType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\CreatorType $creatorType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreatorType $creatorType)
    {
        $validatedData = $request->validate([
            'name' => 'max:255',
            'sort' => 'integer',
        ]);

        $creatorType->update($validatedData);
        return $this->successResponse($creatorType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\CreatorType $creatorType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreatorType $creatorType)
    {
        $creatorType->delete();
        return $this->successResponse($creatorType);
    }

    /**
     * Return list of creator type's names
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNames()
    {
        return $this->successResponse(CreatorType::all()->pluck('name', 'id'));
    }
}
