<?php

namespace App\Http\Controllers;

use App\Creator;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CreatorController extends Controller
{

    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(Creator::paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:1000',
            'bio' => 'max:10000'
        ]);

        $creator = Creator::create($validatedData);
        return $this->successResponse($creator);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Creator $creator
     * @return \Illuminate\Http\Response
     */
    public function show(Creator $creator)
    {
        return $this->successResponse($creator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Creator $creator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Creator $creator)
    {
        $validatedData = $request->validate([
            'name' => 'max:255',
            'description' => 'max:1000',
            'bio' => 'max:10000'
        ]);
        $creator->update($validatedData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Creator $creator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Creator $creator)
    {
        $creator->delete();

        return $this->successResponse($creator);
    }

    /**
     * Return list of creator's names
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNames()
    {
        return $this->successResponse(Creator::all()->pluck('name', 'id'));
    }

}
