<?php

namespace App\Http\Resources;

use App\CreatorType;
use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $result = $this->attributesToArray();
        $result['book_creators'] = $this->getBookCreators();

        return $result;
    }

    private function getBookCreators()
    {
        $bookCreators = [];
        $creatorTypes = CreatorType::orderBy('sort', 'asc')->pluck('name', 'id');
        foreach ($creatorTypes as $creatorTypeId => $creatorType) {
            $bookCreators[$creatorTypeId] = [
                'id' => $creatorTypeId,
                'name' => $creatorTypes[$creatorTypeId],
                'data' => []
            ];
        }

        foreach ($this->creators as $creator) {
            $creatorTypeId = $creator->pivot->creator_type_id;

            $bookCreators[$creatorTypeId]['data'][$creator->id] = [
                'id' => $creator->id,
                'name' => $creator->name
            ];
        }

        $bookCreators = array_filter($bookCreators, function ($creatorType) {
            if (empty($creatorType['data'])) return false;
            return true;
        });

        return $bookCreators;
    }

}
