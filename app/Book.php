<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public $fillable = ['title', 'description', 'isbn', 'year', 'cover'];

    public function authors()
    {
        return $this->belongsToMany(Creator::class)->where('creator_type_id', CreatorType::AUTHOR_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function creators()
    {
        return $this->belongsToMany(Creator::class)->withPivot('creator_type_id');
    }

    public function setCoverAttribute($file)
    {
        if (is_a($file, 'Illuminate\Http\UploadedFile')) {
            $extension = $file->getClientOriginalExtension();
            $filePath = '/uploads/cover/';
            $filename = md5(time() . rand(0, 10000)) . '.' . $extension;
            $file->move(public_path() . $filePath, $filename);

            $this->attributes['cover'] = $filePath . $filename;
        } else {
            $this->attributes['cover'] = $file;
        }

    }

}
