<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('bookyear', function ($attribute, $value, $parameters, $validator) {
            return $value > 1800 && $value < date('Y');
        });
        Validator::extend('isbn', function ($attribute, $value, $parameters, $validator) {
            $passed = true;

            if (!is_numeric($value)) $passed = false;
            if (!in_array(strlen($value), [10, 13])) $passed = false;

            return $passed;
        });

    }
}
